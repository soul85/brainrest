package it.lider.brainrest.dao;

import it.lider.brainrest.entity.Cliente;




//import java.util.ArrayList;
import java.util.List;










import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
//import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



//@Service("clienteDao")
//@Transactional
@Repository
public class ClienteDaoImpl implements ClienteDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	public ClienteDaoImpl() {
		
	}

	// Metodo per l'iniezione della dipendenza della SessionFactory:
	public ClienteDaoImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
        System.out.println("Ho iniettato la SessionFactory: " + sessionFactory);
	}
	
	@Transactional(readOnly = false)
	public void addCliente(Cliente cliente) throws Exception {
		
		System.out.println("inserisci cliente "+cliente.toString());
    	sessionFactory.getCurrentSession().save(cliente);
		
		
	}
	@Transactional
	public Cliente findCliente(String cod_cliente) throws Exception {
		System.out.println("inserisci codice cliente per trovate utente");
    	Cliente cliente=(Cliente)sessionFactory.getCurrentSession().get(Cliente.class, cod_cliente);
		
		return cliente;
		
	}
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<Cliente> getListCliente() throws Exception{
		List<Cliente> listCliente=null;
		
		 Criteria criteria = sessionFactory.getCurrentSession().createCriteria(Cliente.class);
         listCliente = criteria.list(); 
		
		return listCliente;
	}
	
	@Transactional
	public void delete(String cod_cliente) throws Exception{
		System.out.println("cancella cliente");
		Cliente clienteCanc = findCliente(cod_cliente);
		sessionFactory.getCurrentSession().delete(clienteCanc);

	}

	@Transactional
	public void update(Cliente cliente) throws Exception{
		sessionFactory.getCurrentSession().update(cliente);
		
		}
	
	@Transactional
	public boolean verificaId(Cliente cliente) throws Exception {
		if (findCliente(cliente.getCod_cliente())!= null){
			return true;
		}
		else{
			return false;
		}
	}

		
	}
	


