package it.lider.brainrest.dao;

import java.util.List;

import it.lider.brainrest.entity.Cliente;



public interface ClienteDao {
	
	public void addCliente(Cliente cliente)throws Exception;
	public Cliente findCliente(String cod_cliente)throws Exception;
	public List<Cliente> getListCliente()throws Exception;
	public void delete(String cod_cliente)throws Exception;
	public void update(Cliente cliente)throws Exception;
	public boolean verificaId(Cliente cliente)throws Exception;

}
