package it.lider.brainrest.controller;


import it.lider.brainrest.entity.Cliente;
import it.lider.brainrest.service.ServiceDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;




@RestController
public class RestControllerBrain {
	

	@Autowired
	private ServiceDao serviceDao;
	
	@RequestMapping(value = "/cliente/{cod_cliente}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> getCliente(@PathVariable("cod_cliente") String cod_cliente) throws Exception {
		System.out.println("restituisci cliente per codice cliente " + cod_cliente);
		Cliente cliente = serviceDao.findCliente(cod_cliente);
		if (cliente == null) {
			System.out.println("cliente " + cod_cliente + " non trovato");
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/cliente/", method = RequestMethod.POST)
	public ResponseEntity<Void> createCliente(@RequestBody Cliente cliente, UriComponentsBuilder ucBuilder) throws Exception {
		System.out.println("creazione cliente " + cliente.getCod_cliente());

		if (serviceDao.verificaId(cliente)) {
			System.out.println("Cliente con codice " + cliente.getCod_cliente() + " esiste già");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}

		serviceDao.addCliente(cliente);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/cliente/{cod_cliente}").buildAndExpand(cliente.getCod_cliente()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/cliente/{cod_cliente}", method = RequestMethod.DELETE)
    public ResponseEntity<Cliente> deleteCliente(@PathVariable("cod_cliente") String cod_cliente) throws Exception {
        System.out.println("cerca e cancellazione cliente con codice " + cod_cliente);
 
        Cliente cliente = serviceDao.findCliente(cod_cliente);
        if (cliente == null) {
            System.out.println("Impossibile cancellare codice " + cod_cliente + " non trovato");
            return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
        }
 
        serviceDao.delete(cod_cliente);
        return new ResponseEntity<Cliente>(HttpStatus.NO_CONTENT);
    }
	@RequestMapping(value = "/cliente/{cod_cliente}", method = RequestMethod.PUT)
    public ResponseEntity<Cliente> updateUser(@PathVariable("cod_cliente") String cod_cliente, @RequestBody Cliente cliente) throws Exception {
        System.out.println("Updating Cliente " + cod_cliente);
         
        Cliente currentCliente = serviceDao.findCliente(cod_cliente);
         
        if (currentCliente==null) {
            System.out.println("Cliente con codice " + cod_cliente + " non trovato");
            return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
        }
        currentCliente.setId(cliente.getId());
        currentCliente.setArea(cliente.getArea());
        currentCliente.setNome_cliente(cliente.getNome_cliente());
        currentCliente.setNum_punti_vendita(cliente.getNum_punti_vendita());
        currentCliente.setParco_installato_bilance(cliente.getParco_installato_bilance());
        currentCliente.setModelli_bilancia(cliente.getModelli_bilancia());
        currentCliente.setParco_installato_software(cliente.getParco_installato_software());
        currentCliente.setInfrastruttura_it(cliente.getInfrastruttura_it());
        currentCliente.setSistemi_operativi(cliente.getSistemi_operativi());
        currentCliente.setServer_presenti(cliente.getServer_presenti());
        currentCliente.setDatabase_presenti(cliente.getDatabase_presenti());
        
        
        serviceDao.update(currentCliente); 
        
        return new ResponseEntity<Cliente>(currentCliente, HttpStatus.OK);
    }
}
