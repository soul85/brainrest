package it.lider.brainrest.service;

import it.lider.brainrest.dao.ClienteDao;
import it.lider.brainrest.entity.Cliente;

import java.util.List;





import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class ServiceDaoImpl implements ServiceDao {
	
	@Autowired
	ClienteDao clienteDao;
	
	public ServiceDaoImpl(){}
	// Metodo per l'iniezione della dipendenza :
		public ServiceDaoImpl(ClienteDao clienteDao) {
	        this.clienteDao = clienteDao;
	       
		}
		
	@Transactional
	public void addCliente(Cliente cliente) throws Exception {
		
		clienteDao.addCliente(cliente);
	}
	
	@Transactional
	public Cliente findCliente(String cod_cliente) throws Exception {
		
		return clienteDao.findCliente(cod_cliente);
	}
	
	@Transactional
	public List<Cliente> getListCliente() throws Exception {
		
		return clienteDao.getListCliente();
	}
	
	@Transactional
	public void delete(String cod_cliente) throws Exception {
		clienteDao.delete(cod_cliente);
	}
	
	@Transactional
	public void update(Cliente cliente) throws Exception {
		clienteDao.update(cliente);
		
	}
	
	@Transactional
	public boolean verificaId(Cliente cliente) throws Exception {
		return clienteDao.verificaId(cliente);
		
	}

}
