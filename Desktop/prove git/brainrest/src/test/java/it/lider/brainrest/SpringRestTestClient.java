package it.lider.brainrest;

import it.lider.brainrest.entity.Cliente;

import org.springframework.web.client.RestTemplate;

public class SpringRestTestClient {
	
	public static final String REST_SERVICE_URI = "http://localhost:8080/brainrest";
	
	/* GET */
    private static void getCliente(){
        System.out.println("Testing getUser API----------");
        RestTemplate restTemplate = new RestTemplate();
        Cliente cliente = restTemplate.getForObject(REST_SERVICE_URI+"/cliente/su003", Cliente.class);
        System.out.println(cliente.getCod_cliente());
    }

    public static void main(String args[]){
    	
    	getCliente();
    }
}
